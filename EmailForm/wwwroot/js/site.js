﻿function submitForm(button) {
    let $email = $('#email');
    let emailValue = $email[0].value;
    if (validateEmail(emailValue)) {

        let data = $('#form');
        data = data.serialize();
        $.ajax(
            {
                data: data,
                dataType: "html",
                type: "POST",
                url: '',
                success: function () {
                    $email[0].value = '';
                    toastr.success('Email was sent successfully!');
                },
                error: function (error) {
                    toastr.error('Oops, something went wrong. Please try again.');
                }
            });
    }
    else {
        $('#emailValidation').text("Please enter valid email address.");
        let $this = $(button);
        $this.attr('disabled', 'true');       
    }
}

function validateEmail(value) {

    let regex = new RegExp(/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);

    let result = regex.test(value);
    return result;
}
