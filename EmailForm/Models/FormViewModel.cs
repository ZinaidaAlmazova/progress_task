﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EmailForm.Models
{
  public class FormViewModel
    {
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage ="Please enter valid email")]
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
