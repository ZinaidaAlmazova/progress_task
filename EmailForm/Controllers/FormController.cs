﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EmailForm.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EmailForm.Controllers
{
    public class FormController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private bool isResponseOk;

        public FormController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory ?? throw new ArgumentNullException(nameof(clientFactory));
        }

        [HttpGet]
        public ActionResult Submit()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Submit(FormViewModel form)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool isSubmitted = await SubmitFormAsync(form);

                    if (isSubmitted)
                    {
                        return Ok();
                    }

                    return BadRequest();
                }

                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }

        private async Task<bool> SubmitFormAsync(FormViewModel form)
        {
            try
            {
                var url = $"https://us-central1-randomfails.cloudfunctions.net/submitEmail";
                var client = _clientFactory.CreateClient();
                var json = JsonConvert.SerializeObject(form);
                var content = new StringContent(json,
                                                Encoding.UTF8,
                                                "application/json");

                bool response = await SubmitFormRecursivelyAsync(url, client, content, 0);

                return response;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> SubmitFormRecursivelyAsync(string url, HttpClient client, StringContent content, int count)
        {
            //If there is a problem with connection to the external API the recursion has a guaranteed bottom.
            if (isResponseOk || count >= 1000)
            {
                return isResponseOk;
            }
            count++;
            var response = client.PostAsync(url, content);

            Action action = () => isResponseOk = isResponseOk == true ? true : response.Result.StatusCode == System.Net.HttpStatusCode.OK;

            response.GetAwaiter().OnCompleted(action);

            await SubmitFormRecursivelyAsync(url, client, content, count);

            return isResponseOk;
        }
    }
}